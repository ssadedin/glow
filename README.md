# Glow

Glow is a super simple miniature framework for making ncurses-style applications with Groovy

## What is Glow

Glow is a very thin DSL wrapper built on the excellent [Lanterna](https://github.com/mabe02/lanterna). Glow
doesn't expose all of the goodies inside Lanterna which include powerful widgets and layout
managers. Rather, Glow tries to make ultra-simple full screen terminal apps able to be scripted in just
a few lines of code by setting up a default Lanterna full screen context for you, and running a default
keyboard driven event loop. 

Here are the things Glow provides:

- Sets up Lanterna with a full screen terminal context
- Exposese [Screen](http://mabe02.github.io/lanterna/apidocs/3.1/com/googlecode/lanterna/screen/Screen.html), [Terminal](http://mabe02.github.io/lanterna/apidocs/3.1/com/googlecode/lanterna/terminal/Terminal.html) and [TextGraphics](http://mabe02.github.io/lanterna/apidocs/3.1/com/googlecode/lanterna/graphics/TextGraphics.html) objects exposed as `screen`, `terminal` and `graphics` objects respectively. These let you control the screen and draw 
elements like text, lines, boxes with different colors etc easily
- automatically adds an event loop with single key mappings that can be defined as closures 

If you would like to write a simple version of something like `top` (or `htop`), `tig` or other 
ncurses style programs in just a few lines of dynamic script code - Glow could be a good 
tool for you.

## Using Glow

Glow is designed as a Groovy script base class. That means you can write a simple Groovy script
and declare it to use GlowScript as a base class, automatically transforming your script into a
Glow script.

You can use annotations to add Glow as a dependency and set it as your script base class
all inline within your script - so there is no build or install step required for users of
your script. 

Typically you will declare a `draw` closure to render your screen state and a `run` closure to
update the application state based on keyboard inputs. The easiest way to understand this is to
try th example below.

## Example

This example draws some colorful characters that can be moved around in the full screen
by using vi-style navigation keys (h,k,j,l).

```groovy
@GrabResolver(name = 'jitpack.io', root = 'https://jitpack.io')
@Grab('com.gitlab.ssadedin:glow:glow-0.1')

@groovy.transform.BaseScript glow.GlowScript baseScript

x = 2
y = 2                                                                                                                                                                                                                                                                       

draw {
    screen.clear()
    putString(x,y, "XXX")
    bold {
        putString(x,y+1, "YYY")
    }
    withColor(colors.honeyDew2) {
        putString(x, y+2, "===")
    }
    cyan {
        putString(x, y+3, "***")
    }
    red {
        putString(x, y+4, "@@@")
    }
}

run {

    q { exitApp() }

    j { ++y }
    k { --y }
    l { ++x }
    h { --x }
}
```



