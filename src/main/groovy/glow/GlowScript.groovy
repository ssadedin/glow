package glow

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.logging.FileHandler
import java.util.logging.Level
import java.util.logging.LogRecord
import java.util.logging.Logger

import com.googlecode.lanterna.*
import com.googlecode.lanterna.graphics.TextGraphics
import com.googlecode.lanterna.terminal.*
import com.googlecode.lanterna.input.*
import com.googlecode.lanterna.screen.*

import groovy.json.*
import groovy.transform.CompileStatic
import groovy.util.logging.Log

class HandlerBuilder {
    
    Map<String, Closure> handlers = [:]
    
    def methodMissing(String name, args) {
        handlers[name] = args[0]
    }
}

/**
 * A base script that transforms a groovy script a terminal-ui based
 * interactive program.
 *
 * The key concepts involved in creating a terminal UI program are:
 *
 * <li> a closure called <code>draw</code> is responsible for rendering
 *   the current state of the screen using built in Lanterna based
 *   graphics object and also convenience methods directly on this class
 *
 * <li> a <code>run</code> closure implements a "builder" pattern where
 *      unknown method calls are interpreted as keystroke handlers
 *      (so referencing <code>f { ... code ... }</code> creates a 
 *      handler for the <code>f</code> key that calls the given closure
 * <li>a <code>modeStack</code> that swaps out the default key handler
 *     for a default one that can implement alternate keystroke handlers
 *     and can then be "popped" off the stack when done to restore
 *     the previous level key handling. This is useful for switching
 *     into a different screen and then back out again.
 */
abstract class GlowScript extends Script {

    protected static Logger log = Logger.getLogger('GlowScript')
    
    DefaultTerminalFactory defaultTerminalFactory = new DefaultTerminalFactory();

    Terminal terminal = defaultTerminalFactory.createTerminal();
    
    Screen screen
    
    TerminalSize terminalSize 
    
    TextGraphics graphics
    
    List<Closure> modeStack = []
    
    Map<String, Object> colors = [           
        honeyDew2 : new TextColor.Indexed(194),
        primrose : new TextColor.Indexed(181)
    ]
    
    GlowScript() {
    }

    TerminalPosition pos(int x, int y) {
        new TerminalPosition(x,y)
    }

    TerminalSize size(int x, int y) {
        new TerminalSize(x,y)
    }
    
    List<String> buffer = []
    
    boolean stopGlow
    
    Closure render
    
    List args
    
    abstract def runScript()
    
    void run(File file, String [] args) {
        this.args = args as List
        super.run(file, args)
    }
    
    Object run() {
        
        preRun() 
        
        try {
            runScript()
        }
        finally {
            postRun()
        }
    }
    
    def preRun() {
        configureLogging()
        log.info "Starting ..."
        screen = new TerminalScreen(terminal);
        screen.startScreen()
        screen.setCursorPosition(null);
        screen.clear()
        terminalSize = screen.getTerminalSize();
        graphics = screen.newTextGraphics();
    }
    
    def postRun() {
        if(screen != null) {
            screen.stopScreen()
            screen.close()
        }
        println ""
    }
    
    void draw(Closure callback) {
        this.render = callback
    }
    
    void exitApp() {
        this.stopGlow = true
    }
    
    List<List<SGR>> SGRs = [[]]
    
    void style(SGR sgr, Closure callback) {
        SGRs << (SGRs[-1].clone() << sgr)
        callback()
        SGRs.removeLast()
    }
    
    void bold(Closure callback) {
        style(SGR.BOLD, callback)
    }
    
    void red(Closure c) {
        withColor(TextColor.ANSI.RED, c)
    }

    void blue(Closure c) {
        withColor(TextColor.ANSI.BLUE, c)
    }

    void cyan(Closure c) {
        withColor(TextColor.ANSI.CYAN, c)
    }

    void green(Closure c) {
        withColor(TextColor.ANSI.GREEN, c)
    }

    void yellow(Closure c) {
        withColor(TextColor.ANSI.YELLOW , c)
    }

    void withColor(TextColor fg, Closure c) {
        withColors(fg, graphics.backgroundColor, c)
    }

    void withColors(TextColor fg, TextColor bg, Closure c) {
        def oldFg = graphics.foregroundColor
        def oldBg = graphics.backgroundColor
        graphics.setBackgroundColor(bg)
        graphics.setForegroundColor(fg)
        c()
        graphics.setBackgroundColor(oldBg)
        graphics.setForegroundColor(oldFg)
     }
    
    /**
     * Put the given string at the given x,y coordinates using the current default formatting
     *
     * The return value is the x position, designed to enable convenient usage such as
     * 
     * <pre>
     * int col = putString(0, 0, "Hello: ")
     * putString(col, 0, name )
     * </pre>
     */
    int putString(int x, int y, String text) {
        List<SGR> sgrs = SGRs[-1]
        if(sgrs)
            graphics.putString(x,y,text,*sgrs)
        else
            graphics.putString(x,y,text)

        return x + text.size()
    }
    
    boolean autoRender = true
    
    void run(Closure builderCallback) {
        
        log.info "Entering main loop"
        HandlerBuilder builder = new HandlerBuilder()
        builderCallback.delegate = builder
        builderCallback()
        
        this.render()
        screen.refresh()
        
        modeStack << { value ->
            if(builder.handlers.containsKey(value)) {
                log.info "Invoking handler for $value"
                builder.handlers[value].call(value)
            }
            else {
                log.info "No handler for $value from ${builder.handlers*.key}"
            }
            if(autoRender)
                this.render()
            if(!autoRender)
                autoRender = true
        }

        while(!stopGlow) {
            def key = terminal.readInput();
            if(!key?.character)
                continue
            def value = key.keyType == 0x8 ? 0x8 : (key.character as String)
            
            log.info "Stop flag is ${stopGlow}. Type of value is ${value.class?.name}"
            
            modeStack[-1](value)

            screen.refresh()
        }
        log.info "Exit main loop because stop flag triggered"
    }
    
    void inputDialog(Map options = [:], String msg, Closure handler) {
         String dialogMessage = msg
         int rectWidth = 60
         if(options && options.defaultValue) {
            buffer = options.defaultValue as List
         }
         graphics.fillRectangle(pos(17,17), size(rectWidth+2,8), ' ' as char)
         graphics.drawRectangle(pos(18,18), size(rectWidth,6), '*' as char)
         graphics.putString(22,20, msg + buffer.join('').padRight(rectWidth-msg.size()-10, ' '))
         screen.setCursorPosition(pos(22+msg.size()+buffer.size(), 20))
         
         autoRender = false
         
         modeStack << { key ->
            if(key == '\n') {
                def value = buffer.join('')
                buffer = []
                modeStack.removeAt(modeStack.size()-1)
                graphics.fillRectangle(pos(17,17), size(62,8), ' ' as char)
                handler(value)
                screen.setCursorPosition(null)
                render()
                return
            }
            if(key == 0x8) { // backspace
              if(buffer.size()>0)
                  buffer = buffer.take(buffer.size()-1)
            }
            else {
                buffer << key
            }
            graphics.putString(22 + msg.size(),20, buffer.join('').padRight(rectWidth-msg.size()-10, ' '))
            screen.setCursorPosition(pos(22+msg.size()+buffer.size(), 20))
        }
    }
   
    void dialog(String msg, Closure handler) {
         graphics.fillRectangle(pos(17,17), size(62,8), ' ' as char)
         graphics.drawRectangle(pos(18,18), size(60,6), '*' as char)
         graphics.putString(20,20, msg, SGR.BORDERED)
         currentMode = handler
    }
    
    /**
     * The default Java log former uses a format that is too verbose, so
     * replace it with something more compact.
     */
    @CompileStatic
    class SimpleLogFormatter extends java.util.logging.Formatter {
        
        private static final String lineSep = System.getProperty("line.separator");
        
        private static final int DOT = (int)('.' as char)
        
        /**
         * A Custom format implementation that is designed for brevity.
         */
        public String format(LogRecord record) {
            
            DateFormat format = new SimpleDateFormat("h:mm:ss");
        
            String loggerName = record.getLoggerName();
            if(loggerName == null) {
                loggerName = "root";
            }
            
            int dotIndex = loggerName.lastIndexOf(DOT)
            if(dotIndex>=0)
                loggerName = loggerName.substring(dotIndex+1)
                
            StringBuilder output = new StringBuilder()
                .append(loggerName)
                .append("\t[")
                .append(record.threadID)
                .append("]\t")
                .append(record.getLevel()).append("\t|")
                .append(format.format(new Date(record.getMillis())))
                .append(' ')
                .append(record.getMessage()).append(' ')
                .append(lineSep);
                
            if(record.getThrown()!=null) {
                StringWriter w = new StringWriter()
                record.getThrown().printStackTrace(new PrintWriter(w))
                output.append("Exception:\n" + w.toString())
            }    
                
            return output.toString();
        }
    }

    public void configureLogging(Level level = Level.INFO) {
        FileHandler console = new FileHandler(".glow.log")
        console.setFormatter(new SimpleLogFormatter())
        console.setLevel(level)
        Logger log = Logger.getLogger("dummy")
        def parentLog = log.getParent()
        parentLog.getHandlers().each { parentLog.removeHandler(it) }
        log.getParent().addHandler(console)
        
        // Disable logging from groovy sql
        Logger.getLogger("groovy.sql.Sql").useParentHandlers = false
    }
    
}
